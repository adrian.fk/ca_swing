import Controller.FrameManager;
import Controller.FrameManagerInterface;
import Controller.LoginCommand;
import Controller.MainAppCommand;
import Model.DataModel;
import Utils.Loaders.JsonObjectLoader;
import Utils.Loaders.ResourceFetcher;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        ResourceFetcher resourceFetcher = new ResourceFetcher();
        JsonObjectLoader jsonObjectLoader = new JsonObjectLoader();
        try {
            DataModel dataModel = jsonObjectLoader.load(resourceFetcher.getResourceFile().getPath());
            FrameManagerInterface fm = new FrameManager(dataModel, new LoginCommand(), new MainAppCommand());
            fm.launch();

        } catch (IOException e) {
            System.out.println("ERROR: Subjects file could not found.");
        }
    }
}
