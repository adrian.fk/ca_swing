package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataModel {
    private ArrayList<User> users;
    private ArrayList<Subject> subjects;

    public ArrayList<User> getUsers() {
        return users;
    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }

    public Map<String, User> getUsersAsMap() {
        Map<String, User> out = new HashMap<>();
        for(User user : this.users) {
            out.put(user.getName(), user);
        }
        return out;
    }

    public Map<String, Subject> getSubjectsAsIDMap() {
        Map<String, Subject> out = new HashMap<>();
        for(Subject subject : this.subjects) {
            out.put(subject.getId(), subject);
        }
        return out;
    }
}
