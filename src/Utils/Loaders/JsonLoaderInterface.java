package Utils.Loaders;

import Model.DataModel;

interface JsonLoaderInterface {
    public DataModel load(String filePath);
}
