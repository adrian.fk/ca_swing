package Utils.Loaders;

import Model.DataModel;
import com.google.gson.Gson;

import java.io.FileReader;
import java.io.IOException;

public class JsonObjectLoader implements JsonLoaderInterface {

    @Override
    public DataModel load(String filePath) {
        FileReader fileReader;
        DataModel o;
        try {
            fileReader = new FileReader(filePath);
            Gson gson = new Gson();
            o = gson.fromJson(fileReader, DataModel.class);
            return o;
        }
        catch (IOException e) {
            System.out.println("\n\nNo JSON file by that name found");
            throw new Error("No JSON-file found");
        }
    }
}
