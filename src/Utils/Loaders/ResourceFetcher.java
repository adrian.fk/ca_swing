package Utils.Loaders;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ResourceFetcher {
    private final String defaultFilePath = "LSubjects.json";
    private String filePath;

    public ResourceFetcher(String filepath) {
        this.filePath = new String(filepath);
    }

    public ResourceFetcher() {}

    public File getResourceFile() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();

        String path =
                null != this.filePath ?
                      this.filePath :
                      defaultFilePath;

        URL src = classLoader.getResource(path);

        if(src == null) {
            throw new IOException(path + " not found.");
        }
        else {
            return new File(src.getFile());
        }
    }
}
