package View.LoginFrameComponents;

import javax.swing.*;

public class LoginButton extends JButton {
    public LoginButton() {
        super("Login");
        this.setFocusPainted(true);
    }
}
