package View.LoginFrameComponents;

import javax.swing.*;
import java.awt.*;

public class LoginForm extends JPanel {
    private JLabel lUsername;
    private JLabel lPassword;
    private JTextField tfUsername;
    private JPasswordField tfPassword;

    public LoginForm(LayoutManager layout) {
        this.setLayout(layout);
        this.lUsername = new JLabel("Username");
        this.lPassword = new JLabel("Password");
        this.tfUsername = new JTextField();
        this.tfPassword = new JPasswordField();

        this.add(this.lUsername);
        this.add(this.tfUsername);
        this.add(this.lPassword);
        this.add(this.tfPassword);
        this.setVisible(true);
    }

    public String getTfUsername() {
        return tfUsername.getText();
    }

    public String getTfPassword() {
        return new String(tfPassword.getPassword());
    }
}
