package View.MainAppFrameComponents;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class AllSubjectsSubPanel extends SubPanel {
    public AllSubjectsSubPanel(ActionListener controller, String name, String credits, String id, boolean isOptional) {
        super(name, credits, id, isOptional);
        this.setSize(250, 250);
        AllSubjectsFavoriteButton favButton = new AllSubjectsFavoriteButton();
        favButton.setId(id);
        favButton.setPreferredSize(new Dimension(140, 25));
        JPanel bp = new JPanel();
        bp.setBorder(new EmptyBorder(10, 10, 10, 10));
        bp.add(favButton, BorderLayout.CENTER);
        this.add(bp);
        favButton.addActionListener(controller);
    }
}
