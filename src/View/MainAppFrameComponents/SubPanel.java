package View.MainAppFrameComponents;

import javax.swing.*;
import java.awt.*;

public class SubPanel extends JPanel {
        public SubPanel(String name, String credits, String id, boolean isOptional) {

        this.setLayout(new GridLayout(3, 1));

        JLabel lName = new JLabel(name, SwingConstants.CENTER);
        JLabel lCredits = new JLabel(credits + " credits", SwingConstants.CENTER);

        lName.setAlignmentY(Component.CENTER_ALIGNMENT);
        lCredits.setAlignmentY(Component.CENTER_ALIGNMENT);

        this.setBorder(
                BorderFactory.createTitledBorder(
                isOptional ? BorderFactory.createLineBorder(Color.yellow)
                        : BorderFactory.createLineBorder(Color.green),
                id
                )
        );
        this.add(lName);
        this.add(lCredits);
    }
}
