package View.MainAppFrameComponents;

import javax.swing.*;
import java.awt.*;

public class AllSubjectsFavoriteButton extends JButton {
    private final String txtNotClickedState = "Add Favorite";
    private final String txtClickedState = "Remove Favorite";

    private String id;
    private boolean toggled;

    public AllSubjectsFavoriteButton() throws HeadlessException {
        super();
        this.toggled = false;
        processToggledText();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private void processToggledText() {
        if(this.toggled) {
            this.setText(txtClickedState);
        }
        else {
            this.setText(txtNotClickedState);
        }
    }

    public boolean isToggled() {
        return toggled;
    }

    public void setToggled(boolean value) {
        this.toggled = value;
        processToggledText();
    }
}
