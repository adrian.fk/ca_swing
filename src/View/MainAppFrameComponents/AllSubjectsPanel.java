package View.MainAppFrameComponents;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class AllSubjectsPanel extends JPanel {
    public AllSubjectsPanel(List<AllSubjectsSubPanel> allSubjects) {
        this.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.gray),
                "All subjects"
                )
        );
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        for(AllSubjectsSubPanel panel : allSubjects) {
            this.add(panel);
        }
    }
}
