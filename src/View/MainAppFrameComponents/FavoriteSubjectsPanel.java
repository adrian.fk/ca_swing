package View.MainAppFrameComponents;

import javax.swing.*;
import java.awt.*;

public class FavoriteSubjectsPanel extends JPanel {
    public FavoriteSubjectsPanel() {
        this.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.gray),
                "Favorite subjects"
                )
        );
        this.setLayout(new GridLayout(1, 8));
        this.setVisible(true);
    }
}
