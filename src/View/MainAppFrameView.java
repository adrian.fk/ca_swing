package View;

import View.MainAppFrameComponents.AllSubjectsPanel;
import View.MainAppFrameComponents.FavoriteSubjectsPanel;
import View.MainAppFrameComponents.SubPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class MainAppFrameView extends JFrame {
    private FavoriteSubjectsPanel favoriteSubjectsPanel;
    private AllSubjectsPanel allSubjectsPanel;

    public AllSubjectsPanel getAllSubjectsPanel() {
        return allSubjectsPanel;
    }


    public MainAppFrameView(FavoriteSubjectsPanel favoriteSubjectsPanel,
                            AllSubjectsPanel allSubjectsPanel) {
        this.favoriteSubjectsPanel = favoriteSubjectsPanel;
        this.allSubjectsPanel = allSubjectsPanel;

        JPanel content = new JPanel(new GridLayout(2,1));

        content.add(this.favoriteSubjectsPanel);

        JScrollPane scrollPane = new JScrollPane(this.allSubjectsPanel);
        scrollPane.setSize(500, 300);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        content.add(scrollPane);


        this.setSize(900, 700);
        this.getContentPane().add(content);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addFavoriteSubject(SubPanel subject) {
        this.favoriteSubjectsPanel.add(subject);
        this.revalidate();
    }

    public void newList(ArrayList<SubPanel> newList) {
        this.favoriteSubjectsPanel.removeAll();

        for(SubPanel subject : newList) {
            this.favoriteSubjectsPanel.add(subject);
        }

        this.repaint();
    }
}
