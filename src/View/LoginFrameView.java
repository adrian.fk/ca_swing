package View;

import View.LoginFrameComponents.LoginButton;
import View.LoginFrameComponents.LoginForm;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class LoginFrameView extends JFrame {
    private LoginForm loginForm;
    private LoginButton loginButton;

    public LoginFrameView() throws HeadlessException {
        this.setSize(250, 110);
        JPanel content = new JPanel(new BorderLayout());
        loginForm = new LoginForm(new GridLayout(2,2));
        loginButton = new LoginButton();

        content.add(loginForm, BorderLayout.CENTER);
        content.add(loginButton, BorderLayout.PAGE_END);

        this.getRootPane().setDefaultButton(loginButton);
        this.getContentPane().add(content);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    public void attachListeners(ActionListener controller) {
        loginButton.addActionListener(controller);
    }

    public String getFormUsername() {
        return this.loginForm.getTfUsername();
    }

    public String getFormPassword() {
        return this.loginForm.getTfPassword();
    }

    public void displayWrongInput() {
        JOptionPane.showMessageDialog(
                new JFrame(),
                "The credentials are not valid",
                "Warning",
                JOptionPane.WARNING_MESSAGE
        );
    }
}
