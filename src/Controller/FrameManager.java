package Controller;

import Model.DataModel;

import java.util.ArrayList;

public class FrameManager implements FrameManagerInterface {
    private DataModel model;
    private ArrayList<CommandInterface> executions;

    public FrameManager(DataModel model) {
        this.model = model;
        executions = new ArrayList<>();
    }

    public FrameManager(DataModel model, CommandInterface currentExecution) {
        this.model = model;
        executions = new ArrayList<>();
        executions.add(currentExecution);
    }

    public FrameManager(DataModel model, CommandInterface currentExecution, CommandInterface nextExecution) {
        this.model = model;
        executions = new ArrayList<>();
        executions.add(currentExecution);
        executions.add(nextExecution);
    }

    public void launch() {
        CommandInterface launchCommand = executions.get(0);
        if(null != launchCommand) {
            executions.remove(0);
            launchCommand.execute(this, this.model);
        }
    }

    public void launchNext() {
        this.launch();
    }

    public void add(CommandInterface command) {
        executions.add(command);
    }
}
