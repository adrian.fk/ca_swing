package Controller;

import Model.DataModel;

public class MainAppCommand implements CommandInterface {

    @Override
    public void execute(FrameManager fm, DataModel model) {
        MainAppController controller = new MainAppController(model.getSubjectsAsIDMap());
    }
}
