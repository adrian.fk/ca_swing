package Controller;

import Model.User;
import View.LoginFrameView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

public class LoginController implements ActionListener {
    private FrameManagerInterface fm;
    private LoginFrameView view;
    private Map<String, User> users;

    public LoginController(FrameManager fm, Map<String, User> users) {
        this.fm = fm;
        this.users = users;
        this.view = new LoginFrameView();
        this.view.attachListeners(this);
        this.view.setVisible(true);
    }

    private boolean authenticate(String username, String password) {
        return this.users.containsKey(username)
                && this.users.get(username).getPass().equals(password);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(authenticate(this.view.getFormUsername(),
                this.view.getFormPassword())) {

            this.view.setVisible(false);
            fm.launchNext();
        }
        else {
            this.view.displayWrongInput();
        }

    }
}
