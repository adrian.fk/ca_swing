package Controller;

import Model.DataModel;


public class LoginCommand implements CommandInterface {
    @Override
    public void execute(FrameManager fm, DataModel model) {
        LoginController loginController = new LoginController(fm, model.getUsersAsMap());
    }
}
