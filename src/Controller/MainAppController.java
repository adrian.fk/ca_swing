package Controller;

import Model.Subject;
import View.MainAppFrameComponents.*;
import View.MainAppFrameView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainAppController implements ActionListener {
    private MainAppFrameView view;
    private Map<String, Subject> subjectMap;

    private Map<String, AllSubjectsSubPanel> allSubjectsSubPanelMap;
    private Map<String, SubPanel> favoriteSubjectsSubPanelMap;

    public MainAppController(Map<String, Subject> subjectMap) {
        this.subjectMap = subjectMap;
        allSubjectsSubPanelMap = new HashMap<>();
        favoriteSubjectsSubPanelMap = new HashMap<>();

        for(Subject subject : subjectMap.values()) {
            allSubjectsSubPanelMap.put(
                    subject.getId(),
                    new AllSubjectsSubPanel(
                            this,
                            subject.getName(),
                            String.valueOf(subject.getCredits()),
                            subject.getId(),
                            subject.isOptional()
                    )
            );
        }

        AllSubjectsPanel allSubjectsPanel = new AllSubjectsPanel(
                new ArrayList<>(this.allSubjectsSubPanelMap.values())
        );
        FavoriteSubjectsPanel favoriteSubjectsPanel = new FavoriteSubjectsPanel();

        this.view = new MainAppFrameView(favoriteSubjectsPanel, allSubjectsPanel);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        AllSubjectsFavoriteButton button = (AllSubjectsFavoriteButton) actionEvent.getSource();
        if (button.isToggled()) {
            button.setToggled(false);

            String id = button.getId();
            this.favoriteSubjectsSubPanelMap.remove(id);

            this.view.newList(
                    new ArrayList<>(favoriteSubjectsSubPanelMap.values())
            );
        }
        else {
            button.setToggled(true);
            String id = button.getId();
            Subject subject = this.subjectMap.get(id);
            SubPanel subjectPanelElement = new SubPanel(
                    subject.getName(), String.valueOf(subject.getCredits()),
                    subject.getId(), subject.isOptional()
                    );
            favoriteSubjectsSubPanelMap.put(button.getId(), subjectPanelElement);

            this.view.addFavoriteSubject(subjectPanelElement);
        }
    }
}
