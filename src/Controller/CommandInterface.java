package Controller;

import Model.DataModel;

public interface CommandInterface {
    void execute(FrameManager fm, DataModel model);
}
