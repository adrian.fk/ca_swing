package Controller;

public interface FrameManagerInterface {
    void launch();
    void launchNext();
    void add(CommandInterface command);
}
